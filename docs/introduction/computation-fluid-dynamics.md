#  Computational Fluid Dynamics  (CFD) 

```{figure} ../videos/Re25000_l3_bfs_temam_pspg_supg.avi
    :scale: 50%
    :align: left
    :figclass: margin-caption

Blood flow in a cross-section of an artery with Reynolds number (Re) 25000, and with SUPG, PSPG, and Temam stabilization parameters.
```

While you read these words, numerous modern engineering devices are functioning, relying on our understanding of fluid dynamics, either partially or entirely. These devices include airplanes soaring through the sky, ships navigating the seas, cars traveling on roads, and even advanced mechanical biomedical instruments. In our contemporary society, we often take these devices for granted, but it is crucial to pause and acknowledge that each of these machines represents a marvel of modern engineering fluid dynamics. The study of fluid dynamics dates back to ancient times when early civilizations observed the flow of liquids and gases, and thus is too long and out of place to discuss it in this introductory book, nevertheless, I will say a few words about the history of those set of equations that are the corner bricks of many of those modern devices and which the existence, smoothness, and uniquely of their analytical solution is one of the [millennium problems](https://www.claymath.org/millennium/navier-stokes-equation/): the **Navier-Stokes equations (NS)**.




## Brief History

```{figure} ../images/ns_history.png
:align: center
```

The Navier-Stokes equations were formulated by Navier and Stokes in the early 19th century ({cite:p}`navier1822`, {cite:p}`stokes_2009`), they describe (viscous) fluid behavior and they can be seen as the “general” formulations of the Euler equations (appeared in published form in Euler’s article _“Principes généraux du mouvement des fluides”_, published in _Mémoires de l’Académie des Sciences de Berlin_ in 1757 {cite:p}`euler1757principes`[^1]). Their efforts paved the way for modern fluid dynamics, but the complexity of these equations limited their practical use until the latter half of the 20th century[^2] thankful to the advent of (high-speed) computing enabled numerical simulations and thus leading to a new era of computational fluid dynamics (CFD) (example: {cite:p}`kawaguti1953numerical`). It was at this time that the Navier-Stokes equations could really show all their potential, thus allowing fluid dynamics to become increasingly instrumental in understanding natural phenomena and engineering applications alike[^3].


## The equations

```{figure} ../images/nse_sum.png
:align: center
```

Imagine you're exploring the fascinating world of fluid dynamics, where rivers flow, winds blow, and liquids dance in mysterious ways. Behind the scenes of these mesmerizing movements lies a set of equations that govern every incompressible flow, whether it's the smooth flow of a gentle stream or the chaotic swirls of a turbulent storm. These equations are known as the Navier-Stokes equations (NS).

Let's take a peek at the NS equations:

```{admonition} Navier-Stokes Equations
:class: note
$$
\begin{align}
\partial_t \vec{u} + (\vec{u}\cdot\nabla)\vec{u} &= Re^{-1}\nabla^2\vec{u} - \nabla \vec{p} + \vec{f}\\
    \nabla\cdot\vec{u} &= 0
\end{align}
$$
```

Here, $\vec{u}$, $p$, and $f$ are the velocity vector, pressure, and external force, all dimensionless. The mysterious $Re$ is the [**Reynolds number**](https://www.britannica.com/science/Reynolds-number), which provides insights into the flow's nature. In brief, we can see the Reynolds number as the ratio between the convection and diffusion term. 

In the first equation, the term $(\vec{u}\cdot\nabla)\vec{u}$ represents the **convective term**. It describes the transport of momentum due to the bulk motion of the fluid itself. Just as a river carries debris along its flow, this term captures how the velocity field $\vec{u}$ transports its own properties as it moves through space. Think of it as the fluid's way of carrying momentum along its path, contributing to the dynamic motion observed in fluid flows.

On the other hand, the term $Re^{-1}\nabla^2\vec{u}$ represents the **diffusion term**. It accounts for the gradual spreading out or dissipation of momentum within the fluid. While convection carries momentum along with the fluid motion, diffusion works to even out variations in velocity over time. Picture it as the process where chaotic eddies in a turbulent flow gradually smooth out, leading to a more uniform velocity distribution.

By understanding and manipulating these terms within the Navier-Stokes equations, scientists and engineers can unravel the intricate dynamics of fluid flow, from understanding the behavior of ocean currents to optimizing airflow in aircraft design. The Reynolds number acts as a guide, offering insights into whether convection or diffusion dominates a particular flow scenario, shaping our understanding of fluid behavior across various scales.




### Why we should study them and why Computational Mathematics has a vital Role in it?"

Why are these equations such a big deal? Well, they are considered one of the most crucial elements in fluid mechanics. However, there's a catch—finding a general solution is like chasing a unicorn. For most flows, an analytical solution remains elusive, making the Navier-Stokes equations a bit of a puzzle.

Enter the hero: **numerical analysis**. To tackle the complexity posed by the absence of an analytical solution and the coupled relationship between velocity and pressure, a variety of numerical methods come to the rescue. These methods strive to provide an approximate solution, allowing us to unravel the secrets of fluid behavior.

Solving the NS equations involves a simultaneous dance of velocity and pressure, orchestrated by the incompressible constraint $\nabla\cdot\vec{u} = 0 $. It's like solving a puzzle where the pieces (velocity and pressure) need to fit perfectly together.

However, mastering the efficient and reliable numerical solutions of incompressible NS equations is no easy feat. As highlighted in {cite:p}`LANGTANGEN20021125`, it demands skill and precision, especially when applying these solutions for practical purposes.

In essence, the Navier-Stokes equations unveil the poetry behind fluid motion, and the quest to decipher their secrets takes us on a thrilling journey through the realms of mathematics and numerical analysis.

### Derivation of the Navier-Stokes Equations

The equations of fluid flow are governed by conservation laws for mass, momentum and energy, completed by a number of thermodynamic relations.

#### Step 1: Conservation of Mass
Consider a pipe, domain through which fluid flows. The conservation of mass principle tells us that what flows in must flow out. This is expressed mathematically as:

$$\frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \vec{u}) = 0 $$

where:
- $ \rho $ is the fluid density
- $\vec{u}$ is the velocity vector

Don't be scared by the symbols; they just mean "what goes in must come out!"

```{figure} ../images/conservation_mass.png
:align: center
Mass conservation in general setting.
```

```{admonition} Incompressibility and Mass conservation ✨
:class: note
When a fluid is incompressible, its mass density remains constant over time. By applying the divergence theorem, we can connect this concept to mass conservation. 

The divergence theorem states that the integral of the divergence of a vector field over a volume  $\Omega $ is equal to the flux of the vector field through the surface $ \partial \Omega $ of that volume. In the context of fluid dynamics, this theorem implies that what flows into a region must flow out of it.

Mathematically, for an incompressible fluid, this can be expressed as:

$$ \int_\Omega \nabla\cdot\vec{u}\, d\Omega = \int_{\partial\Omega} \vec{u}\cdot n\, d\partial\Omega = 0$$

This equation highlights that the integral of the divergence of the velocity field over the volume $\Omega $ is equal to zero, indicating that the net flow into the volume equals the net flow out of the volume.
```


#### Step 2: Apply Newton's Second Law to a Fluid Element
Now, let's think about what makes the water move. There are three main things:
- Pressure: Pushes the water in different directions.
- Gravity: Pulls the water downward.
- Viscosity: Makes the water sticky, causing friction between layers.


#### Step 3: Putting It All Together
We combine all these ideas into what's called the **momentum equation**:


$$ \frac{\partial (\rho \mathbf{u})}{\partial t} + \nabla \cdot (\rho \mathbf{u} \mathbf{u}) = -\nabla p + \rho \mathbf{g} + \nabla \cdot \tau $$

This equation tells us how the velocity of the water $ \vec{u} $ changes over time due to pressure, gravity, and viscosity.

#### Step 4: Simplifying for Easy Flow
For simpler situations where the water isn't getting squished or stretched too much, we assume it's not getting squished or stretched at all! We call this **incompressible flow**, and it means the density doesn't change much. So, we simplify our equations to deal only with how the water moves. Mathematically we are assuming $ \nabla \cdot \mathbf{u} = 0 $ and neglecting external forces, we simplify the momentum equation to:

$$ \frac{\partial \mathbf{u}}{\partial t} + (\mathbf{u} \cdot \nabla) \mathbf{u} = -\frac{1}{\rho} \nabla p + \nu \nabla^2 \mathbf{u} $$

where $ \nu $ is the kinematic viscosity.




## Discretization

Now, let's talk about discretization. This is a fancy word for breaking down continuous equations (like the Navier-Stokes equations) into smaller, manageable pieces. Think of it like pixelating an image – instead of dealing with every tiny detail, we simplify things to make them easier to work with.

For space discretization, we often turn to either the Finite Elements Method or Finite Volume Method. When dealing with time, we might choose the Finite Difference Method or the Theta Method.

### Finite Elements Method

Think of this like building with Legos. We divide our fluid domain into tiny pieces called elements. Then, we approximate the behavior of the fluid within each element using simple shapes. By piecing together these approximations, we can understand how the fluid moves throughout the entire domain.

### Finite Volume Method

This method is like playing with blocks. We divide our domain into small control volumes, kind of like boxes. Then, we focus on how much fluid flows in and out of each box. By keeping track of these flows and applying principles like conservation of mass, we can figure out how the fluid behaves.

### Finite Difference Method

Imagine you're drawing a map, but instead of curves, you're using straight lines. That's the Finite Difference Method. We approximate derivatives (which show how things change) using simple differences between nearby points. It's like connecting the dots to get a rough idea of the terrain.

#### The Theta/Beta Method 

This is like having a magic slider that controls time. We use it to step forward in time while solving our equations. By adjusting this slider (which we call $\theta$ or $\beta$), we can fine-tune our simulation to be more accurate or stable.


## Further Reading

If you want to delve deeper into the world of mathematics, consider continuing your exploration with the chapter "Mathematics Background." Additionally, you may find valuable insights in the following literature:

* Iserles A. A First Course in the Numerical Analysis of Differential Equations. 2nd ed. Cambridge University Press; 2008. {cite:p}`Iserles_2008`;
* Quarteroni, A., Sacco, R. and Saleri, F., 2006. Numerical mathematics (Vol. 37). Springer Science & Business Media. {cite:p}`quarteroni2006numerical`;
* Elman, H.C., Silvester, D.J. and Wathen, A.J., 2014. Finite elements and fast iterative solvers: with applications in incompressible fluid dynamics. Oxford university press. {cite:p}`elman2014finite`;

[^1]: Although Euler had previously presented his work to the Berlin Academy in 1752 ({cite:p}`christodoulou2007euler`)
[^2]: The first attempt to calculate fluid flow was set forth by Lewis Fry Richardson, with applications for weather prediction ({cite:p}`richardson1922weather`)
[^3]: If you want to know more I advise to read Anderson, 2010 ({cite:p}`anderson2010brief`)  and follow his citations.
