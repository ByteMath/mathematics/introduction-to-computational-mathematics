# Introduction to Applied Mathematics: Shaping Our Modern World 🌐

Applied mathematics stands at the forefront of innovation, driven by the ever-evolving needs of society and the relentless march of scientific progress. Unlike pure mathematics, which explores abstract concepts and theories, applied mathematics is deeply rooted in real-world challenges and advancements.


## The Birth of Applied Mathematics 🚀

The formal discipline of applied mathematics finds its origins in the crucible of World War II, a time when unprecedented logistical and technological challenges demanded a systematic quantitative approach. The Allies, facing monumental hurdles, turned to the collaborative efforts of mathematicians and scientists from diverse disciplines to develop innovative solutions.

Scientific advancements during this period led to breakthroughs in nuclear technology, cryptography, and modern continuum mechanics. Operations research emerged as a new era of rational decision-making, leveraging advanced analytical methods to optimize tasks and processes. This unlikely collaboration laid the groundwork for the structured and interdisciplinary nature of modern scientific disciplines.

## Addressing Contemporary Challenges 🌍

In our modern society, we confront major challenges in climate change, population growth, energy resources, and medicine. Applied mathematics plays a pivotal role in addressing these challenges, serving as an incubator for new ideas and methods.

For decades, climate modeling has drawn heavily from applied mathematics, utilizing theories in dynamical systems, fluid mechanics, and numerical methods. While climate science has evolved into its own discipline, the influence of applied mathematics remains evident, providing a foundation for tackling new and complex problems.

## Embracing the Era of Big Data 📊

Our society is awash in data, with more information generated in recent years than in all of previous history. The rise of big data presents both opportunities and challenges, promising insights into the mysteries of the universe while raising questions about data analysis and interpretation.

Applied mathematics sits at the convergence of collaborative modeling and data research, providing a framework for understanding the underlying mathematical structure of data and methods. As we navigate the era of big data, the interdisciplinary nature of applied mathematics becomes increasingly vital, bridging statistics, computer science, and other mathematical sciences.

## The Enduring Role of Applied Mathematics 🌟

Applied mathematics holds a unique position at the nexus of science and society, driving innovation and shaping our understanding of the world. Its contributions to technology, science, and decision-making underscore its fundamental role in addressing the challenges of today and tomorrow.

As we look to the future, applied mathematics will undoubtedly remain at the center of human endeavors, guiding us toward new discoveries and empowering us to confront the complexities of our ever-changing world.


## Subfields of Applied Mathematics 📚

Applied mathematics encompasses various subfields, each addressing specific mathematical problems or applications:

```{admonition} Computational Mathematics 💻
:class: note, dropdown
Developing numerical methods for solving mathematical problems such as differential equations, integrals, and optimization tasks.
```

```{admonition} Optimization 🎯
:class: note, dropdown
Finding the best solution to a given problem from a set of possible solutions, often using mathematical optimization techniques.
```
```{admonition} Linear Systems and Control 📈
:class: note, dropdown
Analyzing and designing systems governed by linear differential equations, with applications in control theory, signal processing, and robotics.
```

```{admonition} Statistics and Data Science 📊
:class: note, dropdown
Applying mathematical and computational techniques to analyze and interpret data, make predictions, and extract meaningful insights.
```

Lets delve deeper in this fields.

### Computational Mathematics 💻

Computational mathematics plays a fundamental role in applied mathematics, focusing on developing algorithms to solve mathematical problems computationally. Let's explore the key aspects of computational mathematics and its significance.

```{admonition} Accuracy and Efficiency ✨
:class: tip, dropdown
The primary goal of computational mathematics is to develop algorithms that are both accurate and efficient. An accurate algorithm produces results close to the correct analytical solution, while an efficient algorithm solves mathematical problems quickly and effectively using computational resources.
```

```{admonition} Consistency and Stability 🛡️
:class: tip, dropdown
A consistent algorithm consist into an algorithm having the discrete formulation converging to the general problem as we increase the resolution. Additionally, a stable algorithm maintains its accuracy and reliability even when subjected to small changes in input data or parameters. In other words, stability implies that the numerical solution remain bounded.
```

### Optimization 🎯

So… what is mathematical optimization, anyway? “Optimization” comes from the same root as “optimal”, which means best. When you optimize something, you are “making it best”. But “best” can vary. If you’re a football player, you might want to maximize your running yards, and also minimize your fumbles. Both maximizing and minimizing are types of optimization problems.

#### Mathematical Optimization in the “Real World”

Mathematical Optimization is a branch of applied mathematics that finds utility in various fields. It involves the process of making something as "best" as possible. Here are a few examples of where mathematical optimization is applied:

- **Manufacturing**
- **Production**
- **Inventory control**
- **Transportation**
- **Scheduling**
- **Networks**
- **Finance**
- **Engineering**
- **Mechanics**
- **Economics**
- **Control engineering**
- **Marketing**
- **Policy Modeling**

Mathematical Optimization plays a crucial role in solving complex problems in these domains, helping to enhance efficiency, minimize costs, and achieve optimal outcomes.

#### The Dantzig Museum Dilemma: Guard Positioning

The Dantzig Museum faces a complex challenge: how to strategically position guards to ensure security across its 14 rooms interconnected by open doors.

```{figure} ../images/museum.png
:align: center
:scale: 50%
```

With specific requirements for guard coverage and camera surveillance, the museum board seeks mathematician expertise to devise a mathematical model for optimal guard placement.

**Key Considerations:**
- Guards can monitor two adjoining rooms from a single door.
- Certain rooms require double guard coverage, while others can be monitored with cameras.
- The goal is to determine the minimum number of guards needed, their positions, and the rooms requiring camera surveillance.

Through the application of optimization algorithms, it is possible to find a solution like the one below.

```{figure} ../images/museum-solution.png
:align: center
:scale: 50%
```

This is just an easy example of how optimization problems can be used. For example, they are used in file compression and decompression, game theory, and signal processing, particularly image processing, total variation denoising, also known as total variation regulation.

```{figure} ../images/tv-regulation.png
:align: center
:scale: 50%
```

### Linear Systems and Control 📈

Linear Systems and Control deal with the analysis and design of systems governed by linear differential equations. This field has broad applications in control theory, signal processing, and robotics, where understanding and manipulating the behavior of linear systems are crucial. By applying mathematical principles, engineers can design control systems to regulate processes, enhance stability, and achieve desired outcomes in various technological applications.

#### Unraveling the Complexity 🌀

Linear Systems and Control dive into the intricate dance of systems governed by linear differential equations. These systems, found in control theory, signal processing, and robotics, exhibit a particular elegance. Engineers wield the power of mathematical principles to unravel their behavior, a symphony of equations orchestrating seamless coordination.

#### The Symphony of Control 🎶

Imagine you're riding the Segway, the innovative personal transporter. Its speed and stability are not mere coincidences but finely tuned orchestration by control systems. Speed control ensures a smooth and controlled forward motion, while stability control keeps you balanced, adapting to every shift in weight.

```{figure} ../images/segway.png
:align: center
:scale: 50%
[© Segway](http://www.segway.com/). All rights reserved.
```


#### Automation Ballet 🤖

Over the past few decades, road traffic in western society has seen a major increase, presenting both prosperity and challenges. One such challenge is the platooning problem, often occurring on highways where a large group of vehicles meet. Human factors, such as abrupt braking maneuvers, disrupt the smooth flow of traffic, causing significant congestion, safety hazards, air pollution, and increased energy/fuel consumption {cite:p}`sustainable`.

To address this, 'Intelligent Cruise Control' (ICC) has become popular. ICC adjusts a vehicle’s velocity to that of the vehicle in front, either through basic sensors or advanced vehicle-to-vehicle communication, known as 'Cooperative Adaptive Cruise Control' (CACC). This system forms a network of vehicles with synchronized velocities, leading to smoother traffic flow {cite:p}`sustainable`{cite:p}`naus2010string`.

```{figure} ../images/platoon.png
:align: center
:scale: 50%
Block diagram system of a ICC system for controlling vehicles velocities and distances
```

```{figure} ../images/icc-block.png
:align: center
:scale: 50%
Block diagram system of a ICC system for controlling vehicles velocities and distances, ideal form.
```

Research on vehicle platoons has grown significantly, indicating its potential to enhance road safety and fluency. By formulating control laws based on individual vehicle data, such as velocity, acceleration, and distance to preceding vehicles, platoon control systems aim to regulate vehicles to travel together with harmonized velocity and minimal inter-vehicle gaps. This approach shows promise in mitigating traffic congestion, increasing road throughput, and reducing energy consumption {cite:p}`5625054`{cite:p}`sustainable`{cite:p}`naus2010string`.

ICC and platooning research have gained traction, especially with the advent of self-driving cars.

<iframe width="560" height="315" src="https://www.youtube.com/embed/sirzW3AiPhU?si=Onb5Cez9PBmO5LVs&amp;start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Statistics and Data Science 📊

Statistics and Data Science involve the application of mathematical and computational techniques to analyze, interpret, and extract insights from data. This field encompasses statistical methods, machine learning algorithms, and data visualization techniques to uncover patterns, trends, and correlations in large datasets. Statistics and Data Science play a vital role in various domains, including business, healthcare, finance, and social sciences, by enabling data-driven decision-making and predictive modeling for future outcomes.


## Conclusion 🎉

In conclusion, applied mathematics is a dynamic and interdisciplinary field that plays a vital role in understanding, analyzing, and predicting real-world phenomena. By bridging theory and practice, applied mathematicians drive scientific and technological advancements, ensuring that mathematics remains at the forefront of innovation and progress.


## Further Reading

If you want to delve deeper into the world of applied mathematics, consider continuing your exploration this introductory book. Additionally, you may find valuable insights in the following literature:

* Goriely, A., 2018. Applied mathematics: a very short introduction (Vol. 555). Oxford University Press. {cite:p}`goriely2018applied`;
* Casella, G. and Berger, R.L., 2021. Statistical inference. Cengage Learning. {cite:p}`casella2021statistical`;
* Dekking, F.M., Kraaikamp, C., Lopuhaä, H.P. and Meester, L.E., 2005. A Modern Introduction to Probability and Statistics: Understanding why and how (Vol. 488). London: springer. {cite:p}`dekking2005modern`;
* Nocedal, J. and Wright, S.J. eds., 1999. Numerical optimization. New York, NY: Springer New York. {cite:p}`nocedal1999numerical`;
