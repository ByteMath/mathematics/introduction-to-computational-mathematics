# Introduction to Computational Mathematics

This book aims to provide an engaging introduction to computational mathematics through enjoyable simulations. Geared towards newcomers in the field of mathematics and applied mathematics, as well as those unfamiliar with the depth and beauty of mathematics, it serves as a gateway to understanding the subject.

Part of a broader initiative called [ByteMath](https://bytemath.lorenzozambelli.it), this project endeavors to disseminate knowledge about mathematics and its elegance through various mediums such as videos, posts, books, and notebooks.

## Goals

* Gain familiarity with the realm of (computational) mathematics
* Learn to execute your first Python code simulation
* Develop the ability to interpret and discuss simulation results


## Chapters

This book is divided into 3 main components:

* Introduction
* Mathematics background
* Getting started

In the Introduction, we delve into the significance of studying computational mathematics and its applications. Through simulating the Solar System, we explore various fields where computational mathematics plays a crucial role, such as Computational Fluid Dynamics and optimization.

The Mathematics Background section covers essential concepts necessary for simulating the Solar System and introduces fundamental concepts in computational mathematics, including convergence, stability, and iterative algorithms.

In the final chapter, I provide a brief overview of Python and Jupyter, along with instructions on how to locally run this book and its components. For those interested in further exploring Python, refer to the introductory series available [here](https://gitlab.com/ByteMath/python/introduction-to-python).


## Authors

* Lorenzo Zambelli [website](https://lorenzozambelli.it)
