 Introduction to Computational Mathematics

This book aims to provide an engaging introduction to computational mathematics through enjoyable simulations. Geared towards newcomers in the field of mathematics and applied mathematics, as well as those unfamiliar with the depth and beauty of mathematics, it serves as a gateway to understanding the subject.

Part of a broader initiative called [ByteMath](https://bytemath.lorenzozambelli.it), this project endeavors to disseminate knowledge about mathematics and its elegance through various mediums such as videos, posts, books, and notebooks.

## Goals

* Gain familiarity with the realm of (computational) mathematics
* Learn to execute your first Python code simulation
* Develop the ability to interpret and discuss simulation results


## Chapters

This book is divided into 3 main components:

* Introduction
* Mathematics background
* Getting started

In the Introduction, we delve into the significance of studying computational mathematics and its applications. Through simulating the Solar System, we explore various fields where computational mathematics plays a crucial role, such as Computational Fluid Dynamics and optimization.

The Mathematics Background section covers essential concepts necessary for simulating the Solar System and introduces fundamental concepts in computational mathematics, including convergence, stability, and iterative algorithms.

In the final chapter, I provide a brief overview of Python and Jupyter, along with instructions on how to locally run this book and its components. For those interested in further exploring Python, refer to the introductory series available [here](https://gitlab.com/ByteMath/python/introduction-to-python).


## Installation and Local Setup

To utilize this book locally, you'll need to set up Docker on your machine. Follow the steps below to install Docker:

1. **Install Docker:** Depending on your operating system, you can find the installation instructions on the [official Docker website](https://docs.docker.com/get-docker/).

    - For Windows: [Install Docker Desktop for Windows](https://docs.docker.com/desktop/windows/install/).
    - For macOS: [Install Docker Desktop for Mac](https://docs.docker.com/desktop/mac/install/).
    - For Linux: [Install Docker Engine on Linux](https://docs.docker.com/engine/install/).

2. **Running the Book:**
   
   Once Docker is installed, running the book locally is straightforward. Follow these steps:

   - Navigate to the main directory of the project.
   - Run the following command to start the Docker containers:
     ```
     docker-compose up
     ```
   - Next Get Notebook Server Link (something of this form http://0.0.0.0:8888/?token=bcd90816a041fa1f966829d1d46027e4524f40d97b96b8e0 ) 

   ![](docs/images/docker-compose.png)

   - Then in a new terminal run the following command

```bash
docker-compose exec jupyter bash

``` 

![](docs/images/docker-exc.png)


- Next, execute the following commands to clean the cache and build the book:
     ```
     jb clean . && jb build .
     ```
   - After the book is built successfully, access the local server with the notebook by entering the following URL in your browser:
     ```
     http://127.0.0.1:8080/files/_build/html/index.html
     ```
     or, if it does not work, go to the notebook server 

     ![](docs/images/notebook-server.png)

    and navigate to the `_build/html` directory and click on `index.html` to open the book locally.

By following these steps, you'll be able to install Docker, run the book locally, and explore its contents with ease. Happy learning!

Whenever you want to make changes, re run 
```
     jb clean . && jb build .
     ```
as before and reload the localhost page.


## Authors

* Lorenzo Zambelli [website](https://lorenzozambelli.it)


